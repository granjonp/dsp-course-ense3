# Consignes pour le cours de TNS

## <span style="color:red">`News`</span>

#### Séance 2 (mardi 07 avril 2020)

 - avant le cours : faire l'exercice *Choix d'un système d'acquisition* et faire le QCM *ADC - Analog to Digital Conversion*
 - suivre le cours : diapositives *Représentations des signaux discrets*

#### ~~Séance 1 (mardi 31 mars 2020)~~

 - ~~avant le cours : lire les diapositives *Introduction*~~
 - ~~suivre le cours : diapositives *Introduction* et *Les signaux discrets*~~

## Bienvenue à tous !

Vous trouverez sur ce site le matériel nécessaire pour suivre l'enseignement de *Traitement Numérique du Signal* :

- des supports de cours (répertoire *Cours*)
- des exercices (répertoire *Exercices*)
- des quiz (outil en ligne [Socrative](https://b.socrative.com/login/student/))
- les sujet et données de bureau d'étude (archive *DSPLabworks.zip*)

#### Comment s'entrainer aux QCM ?

On utilise l'outil [Socrative](https://b.socrative.com/login/student/) pour les quiz/QCM. Pour y accéder :

1. se connecter à l'outil en tant qu'étudiant https://b.socrative.com/login/student/
2. entrer le nom de la `ROOM` : `GRANJONP`
3. entrer votre nom et prénom (pas de pseudo : ces tests ne sont pas notés, mais ils permettent de vous suivre et de vérifier que vous n'avez pas de problème de compréhension)
4. se déconnecter ('Log out') à la fin du quiz

Vous pouvez refaire le quiz plusieurs fois si vous le souhaitez. Ces quiz seront actualisés en fonction des séances.

#### Comment contacter les profs, poser mes questions, ... ?

On a mis en place une messagerie instantanée (serveur Discord) pour assister aux cours, poser vos questions durant les séances, échanger des documents si besoin...

Connexion au salon réservé pour le cours à l'adresse :

- https://discord.gg/KYSNzvJ

